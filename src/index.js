import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import home from './routes/home'
import ratingLists from './routes/rating-lists'
import { connect } from './db/db'

const
	app = express(),
	port = process.env.PORT || 5000

app.use(cors())

app.use(bodyParser.json())

app.use('/', home)

app.use('/rating-lists', ratingLists)

connect().then(() => app.listen(port, () => {
	global.console.log(`Listening on port '${port}`)
}))

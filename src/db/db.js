import { MongoClient } from 'mongodb'
import { ObjectId } from 'mongodb'

const dbUrl = 'mongodb://ratings:ratings@ds129179.mlab.com:29179/heroku_dkmflhx0'
	
let db

/**
 * @returns {Promise} to connect to the database  
 */
export const connect = () => MongoClient.connect(dbUrl).then(_db => db = _db)

/**
 * @param {String} list name to get the documents from
 * @returns {Promise} to get the documents from the list
 */
export const getDocsFrom = list => db.collection(list).find().toArray()


/**
 * @param {String} name of the list to be added to the database
 * @returns {Promise<String>} to add the list to the database with an id as a resolved value
 */
export const addRatingList = name => Promise.all([
	// add new list to the rating-lists collection
	db.collection('rating-lists').insertOne({ name }).then(doc => doc.insertedId),

	// add new list to the database as a new collection	
	db.createCollection(name)
]).then(values => values[0])


/**
 * @param {String} name of the list to be deleted from the database
 * @returns {Promise} to remove the list form the database
 */
export const removeRatingList = (name, id) => Promise.all([
	db.collection('rating-lists').remove({ _id: ObjectId(id) }),
	db.collection(name).drop()
])


/**
 * @param {String} list to add new item to
 * @param {String} name of the new item to be added
 */
export const addNewDocTo = (list, name) => db.collection(list).count()
	.then(count => db.collection(list).insertOne({ name, rank: count + 1 }))
	.then(doc => doc.ops)


/**
 * @param {String} list to be reordered
 * @param {Array} reorderedDocs array of items with changed ranks
 */
export const reoredDocIn = (list, reorderedDocs) => {
	const updatePromises = reorderedDocs.map(item => (
		db.collection(list).update(
			{ _id: ObjectId(item._id) },
			{ $set: { rank: item.rank } }
		)))

	return Promise.all(updatePromises)
}


/**
 * @param {String} list with item to be deleted
 * @param {String} id of the item to be deleted
 * @returns {Promise} to delete an item from the list
 */
export const removeDocFrom = (list, id) => db.collection(list)
	// find the item to be removed	
	.findOne({ '_id': ObjectId(id) }, { 'rank': 1 })
	// decrease the rank of items, whick are lower in rankings
	.then(itemToRemove => {
		db.collection(list).update(
			{ rank : { $gt : itemToRemove.rank } },
			{ $inc : { rank : -1 } },
			{ multi : true }
		)
	})
	// remove the referenced item from the db
	.then(() => db.collection(list).remove({ _id: ObjectId(id) }))
	// return Promise with updated list of items as a resolve value
	.then(() => db.collection(list).find().toArray())

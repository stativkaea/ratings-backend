import Router from 'express'
import * as db from '../db/db'

const home = Router()

home.get('/', (req, res) => db.getDocsFrom('rating-lists')
	.then(docs => res.json(docs))
	.catch(err => res.status(500).send(err)))

home.post('/', (req, res) => db.addRatingList(req.body.name)
	.then(id => res.status(200).send(id))
	.catch(error => res.status(500).send(error)))

home.delete('/:id', (req, res) => {
	db.removeRatingList('rating-lists', req.params.id)
		.then(() => res.sendStatus(200))
		.catch(error => res.status(500).send(error))
})

export default home

import Router from 'express'
import * as db from '../db/db'

const ratingLists = Router()

ratingLists.get('/:list', (req, res) => db.getDocsFrom(req.params.list)
	.then(docs => res.json(docs))
	.catch(err => res.status(500).send(err)))

ratingLists.post('/:list', (req, res) => db.addNewDocTo(req.params.list, req.body.name)
	.then(doc => res.status(200).json(doc))
	.catch(error => res.status(500).send(error)))

ratingLists.put('/:list/reorder', (req, res) => {
	if (Array.isArray(req.body.reorderedItems)) {
		db.reoredDocIn(req.params.list, req.body.reorderedItems)
			.then(() => res.sendStatus(200))
			.catch(error => res.status(500).send(error))	
	} else {
		res.status(500).send('Incorrect request...')
	}
})

ratingLists.delete('/:list/:id', (req, res) => {
	db.removeDocFrom(req.params.list, req.params.id)
		.then(listItems => res.json(listItems))
		.catch(error => res.status(500).send(error))
})

export default ratingLists
